#define xCAT2(a,b) a##b
#define CAT2(a,b) xCAT2(a,b)

#define xCAT3(a,b,c) a##b##c
#define CAT3(a,b,c) xCAT3(a,b,c)

#define CATPORT(a) CAT2(PORT,a)

#define CATDDR(a) CAT2(DDR,a)