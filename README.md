# Archery Clock (code)

Arduino code to handle displays in archery competitions.
This project was inspired by hjegers's [Archery Clock](http://archeryclock.com) and is compatibile with his
PC software.

With this code panels can be controlled from PC, with wired (USB) or wireless (xBee, nRF24) connections.

**Caution!** This code is not compatibile with the [original Archery Clock
circuit](http://archeryclock.com/Schematics.html) because of pinout changes.

## How can I use it?

You'll need:
- an Arduino Mega;
- Archerino PCB or a handmade one, see [schematics](https://git.golem.linux.it/giuliof/Archerino-schematics);
- (optional) a radio interface (xBee or nRF24L01);
- hjegers's Archery Clock software (crossplatform!).

![Panel assembly](https://golem.linux.it/cloud/index.php/apps/gallery/preview.public/16436?width=1400&height=1400&token=oGPTAkzG3ECkIAz)

## Dependencies

Last stable version has not dependencies, but
- if you need nRF24 radio module you must install [RF24 library](https://github.com/maniacbug/RF24);
- beta version, with LED brightness control, makes use of [TimerOne library](https://github.com/PaulStoffregen/TimerOne);

Please refer to [libraries guide](https://www.arduino.cc/en/Guide/Libraries) for installation.

## Optionals

If you make use of nRF24L01 radio module you must uncomment following line

```
// #define HAS_WIFI
```
then compile the code and load onto Arduino Mega.

## Tech specs

PC (i.e. ArcheryClock application) sends commands to the Arduino to update the LED panel. Every command (package) is formatted as an ASCII number between 0 and 65535 (16-bit), terminated with newline (<code>\n</code>).

Arduino makes integer conversion and parses the command following below specification:

### DigitsValue Package
![Digits Value](docs/DigitsValue.svg)

Identified by first bit equal to zero. Contains information regarding seven segment digits:

- S - side: determines if the package is directed to left or right panel (if presents);
- O - sideOverride: flag the package to be accepted by both panels;
- M - minutes: 
- Right Digit: numeric value of rightmost digit;
- Mid Digit: numeric value of central digit;
- Left Digit: numeric value of leftmost digit;

### TrafficValue Package
![Traffic Value](docs/TrafficValue.svg)

First bit is set to one, followed by two zeros. Contains information regarding traffic lights and archer group letters:

- B - buzzer: turns on the horn;
- Traffic Left: traffic light LEDs for left panel. Bit 4 represents green, followed by orange and red;
- Traffic Right: traffic light LEDs for right panel.
- Archer Groups: each bit, starting from rightmost, represents an archer group from A to F. E and F are currently not supported by the hardware.

### StateValue Package
![State Value](docs/StateValue.svg)

First four bits are set to one. Contains information regarding panel settings and has no direct visive output:

- ABCD: discriminates game phases. If emergency, this is set to 6;
- Competition: expresses the current type of competition (i.e. FITA);
- _three bits are ignored_
- Group Number: number of playing archer groups. Since one group is always present, 0 represent one team, 1 represent two and so on.

### Others
ArcheryClock may send other packages but are ignored by this code due to lack of hardware support.

## Authors

* **Henk Jegers** - *Initial work* - [hjegers](https://github.com/hjegers)
* **Giulio Fieramosca** - *Further developement* - [giuliof](https://github.com/giuliof)

<!-- See also the list of [contributors](https://git.golem.linux.it/giuliof/ArcheryClock-Arduino/contributors) who participated in this project. -->

## License

This project is released under the GPL License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks to
* Giulia of "Arcieri del Poggio" who contributed to first panel version assembly;
* Paolo of "Arcieri Poggibonsi" who acted as beta tester and helped with feedbacks and advices;
